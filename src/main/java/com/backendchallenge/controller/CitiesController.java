package com.backendchallenge.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backendchallenge.model.CityDto;
import com.backendchallenge.service.CitiesService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@EnableSwagger2
public class CitiesController {
	
	public static final String RESPUESTA_EXITOSO = "Servicio invocado correctamente.";
	private static final String MENSAJE_SERVICIO_NO_DISPONIBLE = "Servicio no Disponible";
	private static final String MENSAJE_DATOS_NO_ENCONTRADOS = "No se encontraron datos";
	private static final String MENSAJE_OBTENCION_DATOS = "Obtencion de datos Exitosa";
	
	@Autowired
	private CitiesService citiesService;
	
	@ApiOperation(value = "Cities")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = MENSAJE_OBTENCION_DATOS, response = CityDto.class),
			@ApiResponse(code = 503, message = MENSAJE_SERVICIO_NO_DISPONIBLE, response = CityDto.class),
			@ApiResponse(code = 204, message = MENSAJE_DATOS_NO_ENCONTRADOS, response = CityDto.class) })
	@RequestMapping(value = "/suggestions", method = RequestMethod.GET )
	//@GetMapping(value = "/suggestions/{q}")
	public ResponseEntity<List<CityDto>> consultar(@RequestParam("q") String q , 
			@RequestParam("lat") Optional <Double> lat, @RequestParam("lon") Optional <Double> lon) {
		
		if(lat.isPresent() && lon.isPresent()) {
			List<CityDto> citiesList = citiesService.getSuggestionsCities(q,lat.get(),lon.get());
			if (citiesList != null) {
				return ResponseEntity.status(HttpStatus.OK).body(citiesList);

			} else {

				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
		}
		else
		{
			List<CityDto> citiesList = citiesService.getSuggestionsCities(q);
			if (citiesList != null) {
				return ResponseEntity.status(HttpStatus.OK).body(citiesList);
	
			} else {
	
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
		}

	}

}
