package com.backendchallenge.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityDto {
	public CityDto(String name2, Double latitude2, Double longitude2, Double score2) {
		name = name2;
		latitude = latitude2;
		longitude = longitude2;
		score = score2;
	}
	public CityDto(String name2, Double latitude2, Double longitude2) {
		name = name2;
		latitude = latitude2;
		longitude = longitude2;
	}

	private String name;
	private Double latitude;
	private Double longitude;
	private Double score;
	
}
