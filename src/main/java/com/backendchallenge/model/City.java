package com.backendchallenge.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class City {
	
	private int id;
	private String name;
	private String ascii;
	private String altName;
	private Double latitude;
	private Double longitude;
	private String featClass;
	private String featCode;
	private String country;
	private String cc2;
	private String admin1;
	private String admin2;
	private String admin3;
	private String admin4;
	private	long population;
	private int elevation;
	private int dem;
	private String tz;
	private Date modifiedAt;
}
