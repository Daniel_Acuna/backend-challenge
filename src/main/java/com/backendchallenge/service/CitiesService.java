package com.backendchallenge.service;

import java.util.List;

import com.backendchallenge.model.CityDto;

public interface CitiesService {
	List<CityDto> getSuggestionsCities(String q);
	List<CityDto> getSuggestionsCities(String q, Double latitude, Double longitude);
}
