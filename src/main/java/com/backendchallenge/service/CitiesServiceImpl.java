package com.backendchallenge.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.backendchallenge.model.City;
import com.backendchallenge.model.CityDto;

@Service
public class CitiesServiceImpl implements CitiesService {
	static String formatDate = "yyyy-MM-dd'T'HH:mm:ssZ";
	List<City> citiesList;

	@PostConstruct
	private void ReadCitiesFile() {
		File citiesFile = new File("src/main/resources/cities_canada-usa.tsv");
		citiesList = new ArrayList<>();
		try (BufferedReader TSVReader = new BufferedReader(new FileReader(citiesFile))) {
			String line = null;
			boolean first = true;
			while ((line = TSVReader.readLine()) != null) {
				String[] lineItems = line.split("\t"); // splitting the line and adding its items in String[]
				City currentCity = new City();
				if (first) {
					first = false;
				} else {
					currentCity.setId(Integer.parseInt(lineItems[0]));
					currentCity.setName(lineItems[1]);
					currentCity.setAscii(lineItems[2]);
					currentCity.setAltName(lineItems[3]);
					currentCity.setLatitude(Double.parseDouble(lineItems[4]));
					currentCity.setLongitude(Double.parseDouble(lineItems[5]));
					currentCity.setFeatClass(lineItems[6]);
					currentCity.setFeatCode(lineItems[7]);
					currentCity.setCountry(lineItems[8]);
					currentCity.setCc2(lineItems[9]);
					currentCity.setAdmin1(lineItems[10]);
					currentCity.setAdmin2(lineItems[11]);
					currentCity.setAdmin3(lineItems[12]);
					currentCity.setAdmin4(lineItems[13]);
					currentCity.setPopulation(Long.parseLong(lineItems[14]));
					currentCity.setElevation(lineItems[15].equals("") ? 0 : Integer.parseInt(lineItems[15]));
					currentCity.setDem(Integer.parseInt(lineItems[16]));
					currentCity.setTz(lineItems[17]);
					currentCity.setModifiedAt(stringToDate(lineItems[18]));
					citiesList.add(currentCity);
				}
			}
		} catch (Exception e) {
			System.out.println("Something went wrong");
		}
	}

	@Override
	public List<CityDto> getSuggestionsCities(String q, Double latitude, Double longitude) {


		List<CityDto> citySuggestionsList = citiesList.stream().filter(x -> x.getName().contains(q)
				&& getScore(latitude, longitude, x.getLatitude(),
						  x.getLongitude()) <= 1 && getScore(latitude, longitude, x.getLatitude(),
								  x.getLongitude()) > 0 )
				.map(x -> new CityDto(concat(concat(x.getName(), x.getAdmin1()), x.getCountry()), x.getLatitude(),
						x.getLongitude(), getScore(latitude, longitude, x.getLatitude(), x.getLongitude())))
				.collect(Collectors.toList());
		
		List<CityDto> cities = citySuggestionsList.stream()
				.sorted(Comparator.comparing(a -> a.getScore(), Comparator.nullsLast(Comparator.reverseOrder())))
				.toList();
		return cities;

	}
	
	@Override
	public List<CityDto> getSuggestionsCities(String q) {

		List<CityDto> citySuggestionsList = citiesList.stream().filter(x -> x.getName().contains(q))
				.map(x -> new CityDto(concat(concat(x.getName(), x.getAdmin1()), x.getCountry()), x.getLatitude(),
						x.getLongitude()))
				.collect(Collectors.toList());

		
		return citySuggestionsList;
	}

	private String concat(String cad1, String cad2) {
		return cad1 + "," + cad2;
	}

	private Double getScore(Double lat1, Double lon1, Double lat2, Double lon2) {
		Double R = 6378.137;
		Double dLat = rad(lat2 - lat1);
		Double dLong = rad(lon2 - lon1);
		Double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double d = R * c;
		
		return redondearResultado((1000-d)/1000, 1);
	}
	
    public static double redondearResultado(double numero, int digitos) {
        double resultado;
        resultado = numero * Math.pow(10, digitos);
        resultado = Math.round(resultado);
        resultado = resultado/Math.pow(10, digitos);
        return resultado;
    }

	private Double rad(Double x) {
		return x * Math.PI / 180;
	}

	private static Date stringToDate(String dateBean) {
		SimpleDateFormat formatter = new SimpleDateFormat(formatDate, Locale.ENGLISH);
		formatter.setTimeZone(TimeZone.getTimeZone("America/Mexico_City"));
		Date date;
		try {
			LocalDate localDate = LocalDate.parse(dateBean);
			date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		} catch (Exception ex) {
			return new Date();
		}
		return date;
	}
}
